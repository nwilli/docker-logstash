FROM nwlucas/ubuntu
MAINTAINER Nigel Williams-Lucas

ENV LSTASH_VERSION 1.5.3
ENV PORT_SYSL 5000/udp
ENV PORT_LGFOR 5002
ENV LSTASH_CONF /etc/logstash/conf.d
ENV LSTASH_CERT /etc/logstash/ssl
ENV LSTASH_HOME /opt/logstash
ENV LSTASH_USER logstash
ENV LSTASH_GROUP logstash
ENV LSTASH_PATTERNS /etc/logstash/patterns


LABEL Name="Logstash" Version="${LSTASH_VERSION}" Exposed.ports="${PORT_SYSL} ${PORT_LGFOR}"
LABEL Configuration.directory=$LSTASH_CONF
LABEL Base.OS="Ubuntu" Base.OS.Version="${UBUNTU_VERSION}"

# Logstash installation
# Create a logstash.conf and start logstash by /logstash/bin/logstash agent -f logstash.conf
RUN curl --insecure --progress-bar --remote-name https://download.elastic.co/logstash/logstash/logstash-$LSTASH_VERSION.tar.gz && \
   tar xf logstash-$LSTASH_VERSION.tar.gz && \
   rm logstash-$LSTASH_VERSION.tar.gz && \
   mv logstash-$LSTASH_VERSION $LSTASH_HOME && \
   groupadd -r ${LSTASH_GROUP} && \
   useradd -r -g ${LSTASH_GROUP} -G adm ${LSTASH_USER}


ENV PATH $LSTASH_HOME/bin:$PATH
COPY docker-entrypoint.sh /
COPY conf/ $LSTASH_CONF/
COPY patterns $LSTASH_PATTERNS

RUN chown -R ${LSTASH_USER}:${LSTASH_GROUP} ${LSTASH_HOME}/ ${LSTASH_CONF} ${LSTASH_PATTERNS} && \
    ${LSTASH_HOME}/bin/plugin install logstash-output-elasticsearch && ${LSTASH_HOME}/bin/plugin update

VOLUME ["${LSTASH_CERT}"]
VOLUME ["${LSTASH_CONF}"]

RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE $PORT_SYSL $PORT_LGFOR

CMD ["logstash","-f","/etc/logstash/conf.d"]