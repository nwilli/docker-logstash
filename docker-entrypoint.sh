#!/bin/bash

set -eo pipefail

LSTASH_HOME="/opt/logstash"
LSTASH_CONF_FILE=${LSTASH_CONF_FILE:-"/etc/logstash/conf.d/logstash.conf"}
# Logstash-Forwarder setup
LSTASH_SSL_CERT=${LSTASH_SSL_CERT:-"/etc/logstash/ssl/logstash-forwarder.crt"}
LSTASH_SSL_KEY=${LSTASH_SSL_KEY:-"/etc/logstash/ssl/logstash-forwarder.key"}

createLSKey() {
# Create a new SSL certificate for Logstash-Forwarder if needed
  if [ ! -f "$LSTASH_SSL_KEY" ]; then
    echo "[LS] Generating new logstash-forwarder key"
    openssl req -x509 -batch -nodes -newkey rsa:4096 -keyout "$LSTASH_SSL_KEY" -out "$LSTASH_SSL_CERT" &>/dev/null
  fi
}

checkLSConfig() {
  # Check/validate config file
  ${LSTASH_HOME}/bin/logstash agent -t -f ${LSTASH_CONF_FILE}
  if [ $? -ne 0 ]; then
    echo "[LS] Invalid ${LSTASH_CONF_FILE} file, exiting."
    exit 1
  elif [[ $? == "Configuration OK" ]]; then
    echo "[LS] Valid ${LSTASH_CONF_FILE} file."
  fi
}

# Add logstash as command if needed
if [[ "$1" == -* ]]; then
  createLSKey
  checkLSConfig
  set -- logstash "$@"
fi

# Run as user "logstash" if the command is "logstash"
if [ "$1" == logstash ]; then
  createLSKey
  checkLSConfig
  set -- gosu logstash "$@"
fi

exec "$@"